#include "LArCafJobs/LArShapeDumperTool.h"
#include "LArCafJobs/LArShapeDumper.h"
#include "LArCafJobs/LArSimpleShapeDumper.h"
#include "LArCafJobs/LArNoiseBursts.h"
#include "LArCafJobs/LArHECNoise.h"

DECLARE_COMPONENT( LArShapeDumper )
DECLARE_COMPONENT( LArSimpleShapeDumper )
DECLARE_COMPONENT( LArNoiseBursts )
DECLARE_COMPONENT( LArHECNoise )
DECLARE_COMPONENT( LArShapeDumperTool )

